package ru.redcollar.store.testAuth;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import ru.redcollar.store.dto.LoginFormDto;
import ru.redcollar.store.dto.TokenDto;
import ru.redcollar.store.dto.account.AccountCreationDto;

import static org.springframework.test.util.AssertionErrors.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class TestAuthentication {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Sql("classpath:db/init_account.sql")
    public void successfulLoginShouldReturnToken() throws Exception {
        LoginFormDto loginForm = new LoginFormDto();
        loginForm.setEmail("test@email.com");
        loginForm.setPassword("testPassword");
        MockHttpServletResponse response = mockMvc.perform(
                        post("/auth/login")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(OBJECT_MAPPER.writeValueAsString(loginForm))
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse();

        TokenDto tokenDto = OBJECT_MAPPER.convertValue(response.getContentAsString(), TokenDto.class);
        assertNotNull("Token NotNull assertion", tokenDto.getToken());
    }

    @Test
    @Sql("classpath:db/init_user_role.sql")
    public void successfulRegistrationForUser() throws Exception {
        AccountCreationDto account = new AccountCreationDto();
        account.setName("testName");
        account.setEmail("testMail");
        account.setPassword("testPwd");
        mockMvc.perform(
                        post("/auth/register")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(OBJECT_MAPPER.writeValueAsString(account))
                )
                .andExpect(status().isCreated());
    }
}
