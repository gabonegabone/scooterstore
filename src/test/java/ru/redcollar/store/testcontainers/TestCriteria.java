package ru.redcollar.store.testcontainers;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.redcollar.store.entity.Purchase;
import ru.redcollar.store.pagination.PurchasePaginationSpecifications;
import ru.redcollar.store.pagination.PurchaseSpecificationBuilder;
import ru.redcollar.store.repository.PurchaseRepository;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@ActiveProfiles(profiles = "test-containers")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestCriteria {

    private final PurchaseRepository purchaseRepository;

    @Test
    @Sql({"classpath:db/init_account.sql", "classpath:db/init_criteria_between_dates_test.sql"})
    @Sql(scripts = "classpath:db/truncate.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldReturnPurchasesBetweenDates() {
        PurchasePaginationSpecifications filter = new PurchasePaginationSpecifications();
        filter.setFromDate(LocalDate.of(2021, 12, 15));
        filter.setToDate(LocalDate.of(2021, 12, 26));
        Specification<Purchase> spec = PurchaseSpecificationBuilder.build(filter);
        List<Purchase> result = purchaseRepository.findAll(spec);
        assertTrue("", result.size() == 2);
    }

    @Test
    @Sql({"classpath:db/init_account.sql", "classpath:db/init_criteria_exact_product_test.sql"})
    @Sql(scripts = "classpath:db/truncate.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldReturnPurchaseWithExactProductName() {
        PurchasePaginationSpecifications filter = new PurchasePaginationSpecifications();
        filter.setProductName("correct");
        Specification<Purchase> spec = PurchaseSpecificationBuilder.build(filter);
        List<Purchase> result = purchaseRepository.findAll(spec);
        assertTrue("", result.size() == 2);
    }

    @Test
    @Sql({"classpath:db/init_account.sql", "classpath:db/init_criteria_between_dates_test.sql"})
    @Sql(scripts = "classpath:db/truncate.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void shouldPaginateCriteriaQuery() {
        PurchasePaginationSpecifications filter = new PurchasePaginationSpecifications();
        filter.setFromDate(LocalDate.of(2021, 12, 15));
        filter.setToDate(LocalDate.of(2021, 12, 26));
        Pageable pageable = PageRequest.of(0, 1, Sort.Direction.ASC, "id");
        Specification<Purchase> spec = PurchaseSpecificationBuilder.build(filter);
        Page<Purchase> result = purchaseRepository.findAll(spec, pageable);
        assertEquals("", 2, result.getTotalPages());
        assertEquals("", 1, result.getSize());
    }
}
