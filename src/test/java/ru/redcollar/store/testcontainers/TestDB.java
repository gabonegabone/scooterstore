package ru.redcollar.store.testcontainers;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.redcollar.store.entity.Account;
import ru.redcollar.store.entity.Purchase;
import ru.redcollar.store.entity.Role;
import ru.redcollar.store.repository.AccountRepository;
import ru.redcollar.store.repository.PurchaseRepository;
import ru.redcollar.store.repository.RoleRepository;

import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@ActiveProfiles(profiles = "test-containers")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TestDB {

    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final PurchaseRepository purchaseRepository;

    @Test
    public void shouldCreateAdmin() {
        Account adminAccount = new Account();
        adminAccount.setName("admin");
        Role adminRole = new Role();
        adminRole.setName("ADMIN");
        adminRole = roleRepository.saveAndFlush(adminRole);
        adminAccount.setRoles(List.of(adminRole));
        Account result = accountRepository.saveAndFlush(adminAccount);
        assertEquals("result has a name of admin", "admin", result.getName());
        assertTrue("result has an adminRole", result.getRoles().contains(adminRole));
    }

    @Test
    public void shouldPaginate() {
        Account account = new Account();
        account = accountRepository.saveAndFlush(account);
        for (int i = 0; i < 4; i++) {
            Purchase purchase = new Purchase();
            purchase.setAccount(account);
            purchaseRepository.save(purchase);
        }
        purchaseRepository.flush();

        Pageable pageable = PageRequest.of(0, 3, Sort.Direction.ASC, "id");
        Page<Purchase> result = purchaseRepository.findAll(pageable);
        assertEquals("should return page of 3 entities", 3, result.getContent().size());
    }
}
