package ru.redcollar.store.testPurchase;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import ru.redcollar.store.dto.product.ProductDto;
import ru.redcollar.store.dto.purchase.PurchaseDto;
import ru.redcollar.store.dto.purchaseproduct.PurchaseProductDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
public class TestPurchase {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    private PurchaseDto purchaseDto;

    @BeforeEach
    public void setupPurchaseDto() {
        log.info("initializing PurchaseDto...");
        ProductDto product = new ProductDto();
        product.setId(1L);

        PurchaseProductDto purchaseProduct = new PurchaseProductDto();
        purchaseProduct.setProduct(product);
        purchaseProduct.setQuantity(1337);

        List<PurchaseProductDto> purchaseProductList = new ArrayList<>();
        purchaseProductList.add(purchaseProduct);

        purchaseDto = new PurchaseDto();
        purchaseDto.setPurchaseProducts(purchaseProductList);
        log.info("PurchaseDto initialized");
    }

    @Test
    @Sql({"classpath:db/init_product.sql", "classpath:db/init_account.sql"})
    @WithMockUser
    public void checkoutShouldCreatePurchase() throws Exception {
        //setting correct user principal
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Collection<GrantedAuthority> authorities = user.getAuthorities();
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(
                        1L,
                        "testEmail",
                        authorities
                )
        );
        //tests
        MockHttpServletResponse response =
                mockMvc.perform(post("/purchase/checkout")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(OBJECT_MAPPER.writeValueAsString(purchaseDto)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse();

        JsonNode purchaseNode = OBJECT_MAPPER.readTree(response.getContentAsString());
        String resultPurchaseStatus = purchaseNode.get("status").asText();
        assertTrue("Assert resulting purchase status is PROCESSING",
                resultPurchaseStatus.equals("PROCESSING"));
    }

    @Test
    public void checkoutShouldNotAccessibleForNotAuthorized() throws Exception {
        mockMvc.perform(post("/purchase/checkout")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(purchaseDto)))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    public void checkoutShouldNotAccessibleForNotUsers() throws Exception {
        mockMvc.perform(post("/purchase/checkout")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(purchaseDto)))
                .andExpect(status().isForbidden());
    }
}
