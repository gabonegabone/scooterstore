insert into product (id, name) values (1, 'correct');
insert into product (id, name) values (2, 'wrong');

insert into purchase (id, account_id) values (1, 1);
insert into purchase (id, account_id) values (2, 1);
insert into purchase (id, account_id) values (3, 1);

insert into purchase_product (purchase_id, product_id) values (1, 1);
insert into purchase_product (purchase_id, product_id) values (1, 2);
insert into purchase_product (purchase_id, product_id) values (2, 1);
insert into purchase_product (purchase_id, product_id) values (3, 2);