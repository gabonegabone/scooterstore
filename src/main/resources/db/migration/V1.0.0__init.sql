CREATE TABLE account
(
    id       SERIAL PRIMARY KEY,
    name     VARCHAR(40),
    email    VARCHAR(40),
    password VARCHAR(255)
);

CREATE TABLE role
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(255),
    description VARCHAR(255)
);

CREATE TABLE account_role
(
    account_id SERIAL NOT NULL,
    role_id    SERIAL NOT NULL,

    CONSTRAINT PK_ACCOUNT_ROLE PRIMARY KEY (account_id, role_id),
    CONSTRAINT FK_ACCOUNT_ROLE_ACCOUNT_ID
        FOREIGN KEY (account_id)
            REFERENCES account (id),

    CONSTRAINT FK_ACCOUNT_ROLE_ROLE_ID
        FOREIGN KEY (role_id)
            REFERENCES role (id)
);

INSERT INTO role (id, name, description)
VALUES (1, 'USER', 'buy items');
INSERT INTO role (id, name, description)
VALUES (2, 'ADMIN', 'operate with accounts and items');

CREATE TABLE product
(
    id             SERIAL PRIMARY KEY,
    name           VARCHAR(255),
    price          DECIMAL,
    color          VARCHAR(255),
    bar_height     INT,
    deck_length    INT,
    deck_width     INT,
    wheel_diameter INT,
    wheel_width    INT,
    compression    VARCHAR(255),
    material       VARCHAR(255),
    bars_backsweep BOOLEAN
);

CREATE TABLE storage
(
    id   SERIAL PRIMARY KEY,
    info VARCHAR(255)
);

CREATE TABLE storage_product
(
    id         SERIAL PRIMARY KEY,
    storage_id BIGINT NOT NULL,
    product_id BIGINT NOT NULL,
    quantity   INT    NOT NULL,
    CONSTRAINT fk_product_storage_storage_id
        FOREIGN KEY (storage_id)
            REFERENCES storage (id),
    CONSTRAINT fk_storage_product_product_id
        FOREIGN KEY (product_id)
            REFERENCES product (id)
);

CREATE TABLE purchase
(
    id         SERIAL PRIMARY KEY,
    account_id BIGINT NOT NULL,
    date       DATE,
    status     VARCHAR(50),
    CONSTRAINT fk_purchase_account
        FOREIGN KEY (account_id)
            REFERENCES account (id)
);

CREATE TABLE purchase_product
(
    id          SERIAL PRIMARY KEY,
    purchase_id BIGINT NOT NULL,
    product_id  BIGINT NOT NULL,
    quantity    INT    NOT NULL,
    CONSTRAINT fk_purchase_has_product_purchase1
        FOREIGN KEY (purchase_id)
            REFERENCES purchase (id),
    CONSTRAINT fk_purchase_has_product_product1
        FOREIGN KEY (product_id)
            REFERENCES product (id)
);