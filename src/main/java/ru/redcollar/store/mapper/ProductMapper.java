package ru.redcollar.store.mapper;

import org.mapstruct.Mapper;
import ru.redcollar.store.dto.product.ProductDto;
import ru.redcollar.store.entity.Product;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product parseToProduct(ProductDto productDto);

    ProductDto parseToDto(Product product);
}
