package ru.redcollar.store.mapper;

import org.mapstruct.Mapper;
import ru.redcollar.store.dto.purchaseproduct.PurchaseProductDto;
import ru.redcollar.store.entity.PurchaseProduct;

@Mapper(componentModel = "spring")
public interface PurchaseProductMapper {

    PurchaseProductDto parseToDto(PurchaseProduct entity);

    PurchaseProduct parseToEntity(PurchaseProductDto dto);
}
