package ru.redcollar.store.mapper;

import org.mapstruct.Mapper;
import ru.redcollar.store.dto.purchase.PurchaseDto;
import ru.redcollar.store.entity.Purchase;

@Mapper(componentModel = "spring")
public interface PurchaseMapper {

    PurchaseDto parseToDto(Purchase purchase);

    Purchase parseToEntity(PurchaseDto purchaseDto);
}
