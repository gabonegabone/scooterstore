package ru.redcollar.store.mapper;

import org.mapstruct.Mapper;
import ru.redcollar.store.dto.storage.StorageProductDto;
import ru.redcollar.store.entity.StorageProduct;

@Mapper(componentModel = "spring")
public interface StorageProductMapper {

    StorageProduct parseToEntity(StorageProductDto storageProductDto);

    StorageProductDto parseToDto(StorageProduct storageProduct);
}
