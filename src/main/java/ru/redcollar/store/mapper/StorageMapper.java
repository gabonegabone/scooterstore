package ru.redcollar.store.mapper;

import org.mapstruct.Mapper;
import ru.redcollar.store.dto.storage.StorageDto;
import ru.redcollar.store.entity.Storage;

@Mapper(componentModel = "spring")
public interface StorageMapper {

    Storage parseToEntity(StorageDto storageDto);

    StorageDto parseToDto(Storage storage);
}
