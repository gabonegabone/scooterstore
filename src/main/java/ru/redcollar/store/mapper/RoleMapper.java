package ru.redcollar.store.mapper;

import org.mapstruct.Mapper;
import ru.redcollar.store.dto.role.RoleDto;
import ru.redcollar.store.entity.Role;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleDto parseToDto(Role role);

    Role parseToEntity(RoleDto roleDto);
}