package ru.redcollar.store.mapper;

import org.mapstruct.Mapper;
import ru.redcollar.store.dto.account.AccountCreationDto;
import ru.redcollar.store.dto.account.AccountDto;
import ru.redcollar.store.entity.Account;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    Account parseToEntity(AccountCreationDto accountDto);

    Account parseToEntity(AccountDto accountDto);

    AccountDto parseToDto(Account account);
}
