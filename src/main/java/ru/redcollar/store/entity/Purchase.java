package ru.redcollar.store.entity;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Purchase {

    public enum Status {
        COMPLETED, DELIVERING, PROCESSING, DENIED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    @OneToMany(mappedBy = "purchase")
    private List<PurchaseProduct> purchaseProducts;

    @Column(name = "date")
    private LocalDate checkoutDate;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    public Purchase(Long id) {
        this.id = id;
    }
}
