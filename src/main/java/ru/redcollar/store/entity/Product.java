package ru.redcollar.store.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
public class Product {

    public enum Color {
        BLACK, GREEN, YELLOW, WHITE, RED_AND_BLACK
    }

    public enum Compression {
        HIC, iHC, SCS, iCS
    }

    public enum Material {
        ALUMINIUM_7076T, ALUMINIUM_5076T
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private BigDecimal price;

    @Enumerated(EnumType.STRING)
    private Color color;

    @Column(name = "bar_height")
    private Integer barHeight;

    @Column(name = "deck_length")
    private Integer deckLength;

    @Column(name = "deck_width")
    private Integer deckWidth;

    @Column(name = "wheel_diameter")
    private Integer wheelDiameter;

    @Column(name = "wheel_width")
    private Integer wheelWidth;

    @Enumerated(EnumType.STRING)
    private Compression compression;

    @Enumerated(EnumType.STRING)
    private Material material;

    @Column(name = "bars_backsweep")
    private Boolean barsBacksweep;

}
