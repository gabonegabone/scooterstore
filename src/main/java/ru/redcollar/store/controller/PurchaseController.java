package ru.redcollar.store.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.redcollar.store.dto.purchase.PurchaseDto;
import ru.redcollar.store.pagination.PurchasePaginationSpecifications;
import ru.redcollar.store.service.PurchaseService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/purchase")
@RequiredArgsConstructor
public class PurchaseController {

    private final PurchaseService purchaseService;

    @RolesAllowed("ROLE_USER")
    @PostMapping("/checkout")
    public ResponseEntity<PurchaseDto> checkout(@RequestBody PurchaseDto purchaseDto) {
        PurchaseDto dto = purchaseService.checkout(purchaseDto);
        return ResponseEntity.ok(dto);
    }

    @RolesAllowed("ROLE_ADMIN")
    @GetMapping("/list")
    public ResponseEntity<List<PurchaseDto>> getList(@RequestBody @Valid PurchasePaginationSpecifications filter) {
        List<PurchaseDto> list = purchaseService.getList(filter);
        return ResponseEntity.ok(list);
    }

    @RolesAllowed("ROLE_ADMIN")
    @PutMapping
    public ResponseEntity<PurchaseDto> edit(@RequestBody PurchaseDto purchaseDto) {
        PurchaseDto dto = purchaseService.edit(purchaseDto);
        return ResponseEntity.ok(dto);
    }
}
