package ru.redcollar.store.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.redcollar.store.dto.KeycloakToken;
import ru.redcollar.store.dto.KeycloakUser;
import ru.redcollar.store.service.KeycloakAuthService;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
@Slf4j
public class CacheTestController {

    private final KeycloakAuthService keycloakAuthService;

    @PostMapping
    public ResponseEntity<KeycloakToken> tokenCache(@RequestBody KeycloakUser user) {
        KeycloakToken token = keycloakAuthService.getToken(user);
        log.info("token {}", token);
        return ResponseEntity.ok(token);
    }
}
