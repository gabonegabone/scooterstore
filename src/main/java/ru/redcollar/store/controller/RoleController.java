package ru.redcollar.store.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.redcollar.store.dto.role.RoleDto;
import ru.redcollar.store.service.RoleService;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/role")
@RolesAllowed("ROLE_ADMIN")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @GetMapping("/list")
    public ResponseEntity<List<RoleDto>> getList() {
        List<RoleDto> list = roleService.getList();
        return ResponseEntity.ok(list);
    }
}
