package ru.redcollar.store.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.redcollar.store.dto.LoginFormDto;
import ru.redcollar.store.dto.TokenDto;
import ru.redcollar.store.dto.account.AccountCreationDto;
import ru.redcollar.store.service.AuthenticationService;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<TokenDto> login(@RequestBody LoginFormDto loginFormDto) {
        TokenDto token = authenticationService.login(loginFormDto);
        return ResponseEntity.ok(token);
    }

    @PostMapping("/register")
    public ResponseEntity<Void> register(@RequestBody AccountCreationDto accountCreationDto) {
        authenticationService.register(accountCreationDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}