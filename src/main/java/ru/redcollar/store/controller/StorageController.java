package ru.redcollar.store.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.redcollar.store.dto.storage.StorageDto;
import ru.redcollar.store.service.StorageService;

import java.util.List;

@RestController
@RequestMapping("/storage")
@RequiredArgsConstructor
public class StorageController {

    private final StorageService storageService;

    @GetMapping
    public ResponseEntity<List<StorageDto>> list() {
        List<StorageDto> list = storageService.getList();
        return ResponseEntity.ok(list);
    }

    @PostMapping
    public ResponseEntity<StorageDto> save(@RequestBody StorageDto storageDto) {
        StorageDto dto = storageService.save(storageDto);
        return ResponseEntity.ok(dto);
    }

    @PutMapping
    public ResponseEntity<StorageDto> update(@RequestBody StorageDto storageDto) {
        StorageDto dto = storageService.update(storageDto);
        return ResponseEntity.ok(dto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {
        storageService.delete(id);
        return ResponseEntity.ok().build();
    }
}
