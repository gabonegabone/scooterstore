package ru.redcollar.store.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.redcollar.store.dto.account.AccountCreationDto;
import ru.redcollar.store.dto.account.AccountDto;
import ru.redcollar.store.dto.account.AccountPasswordEditingDto;
import ru.redcollar.store.entity.Account;
import ru.redcollar.store.service.AccountService;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/account")
@RolesAllowed("ROLE_ADMIN")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @Operation(summary = "Get list of accounts")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "non empty list returned"
            ),
            @ApiResponse(
                    responseCode = "204",
                    description = "list is empty"
            )
    })
    @GetMapping("/list")
    public ResponseEntity<List<AccountDto>> getList() {
        List<AccountDto> list = accountService.getList();
        return ResponseEntity.ok(list);
    }

    @PostMapping
    public ResponseEntity<AccountDto> createAccount(@RequestBody AccountCreationDto newAccount) {
        AccountDto accountDto = accountService.createAccount(newAccount);
        return ResponseEntity.status(HttpStatus.CREATED).body(accountDto);
    }

    @PutMapping
    public ResponseEntity<AccountDto> editAccount(@RequestBody AccountDto newAccount) {
        AccountDto accountDto = accountService.editAccount(newAccount);
        return ResponseEntity.ok(accountDto);
    }

    @PutMapping("/password")
    public ResponseEntity<AccountDto> editPassword(@RequestBody AccountPasswordEditingDto newAccount) {
        AccountDto accountDto = accountService.editPassword(newAccount);
        return ResponseEntity.ok(accountDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteAccount(@PathVariable("id") Long id) {
        accountService.deleteAccount(id);
        return ResponseEntity.ok().build();
    }
}