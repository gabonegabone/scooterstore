package ru.redcollar.store.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.redcollar.store.dto.product.ProductDto;
import ru.redcollar.store.exception.NoIdFoundException;
import ru.redcollar.store.service.ProductService;

import javax.annotation.security.RolesAllowed;
import javax.xml.transform.OutputKeys;
import java.util.List;

@RolesAllowed("ROLE_ADMIN")
@RequestMapping("/product")
@RestController
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
    @GetMapping("/list")
    public ResponseEntity<List<ProductDto>> getList() {
        List<ProductDto> list = productService.getList();
        return ResponseEntity.ok(list);
    }

    @PostMapping
    public ResponseEntity<ProductDto> create(@RequestBody ProductDto newProduct) {
        ProductDto productDto = productService.create(newProduct);
        return ResponseEntity.ok(productDto);
    }

    @PutMapping
    public ResponseEntity<ProductDto> edit(@RequestBody ProductDto newProduct) {
        ProductDto productDto = productService.edit(newProduct);
        return ResponseEntity.ok(productDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {
            productService.delete(id);
        return ResponseEntity.ok().build();
    }
}
