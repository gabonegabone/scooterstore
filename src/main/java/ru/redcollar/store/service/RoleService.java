package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.redcollar.store.dto.role.RoleDto;
import ru.redcollar.store.entity.Role;
import ru.redcollar.store.exception.NoContentException;
import ru.redcollar.store.exception.NoIdFoundException;
import ru.redcollar.store.mapper.RoleMapper;
import ru.redcollar.store.repository.RoleRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class RoleService {

    private static final Role USER_ROLE = new Role(1L);

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    public List<RoleDto> getList() {
        List<Role> roles = roleRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        if (roles.isEmpty()) {
            NoContentException exc = new NoContentException(Role.class);
            log.error("Role service: " + exc.getMessage());
            throw exc;
        }
        log.info("Role service: getList() - success");
        return roles.stream().map(roleMapper::parseToDto).collect(Collectors.toList());
    }

    public List<Role> findByIds(List<Long> roleIds) {
        List<Role> roles = roleRepository.findByIds(roleIds);
        if (roles.isEmpty()) {
            NoContentException exc = new NoContentException(Role.class);
            log.error("Role service: " + exc.getMessage());
            throw exc;
        }
        log.info("Role service: roles with ids: {} has been returned", roleIds);
        return roles;
    }

    public List<Role> parseRoles(List<RoleDto> roleDtos) {
        List<Role> roles;
        if (roleDtos == null) {
            roles = new ArrayList<>();
            roles.add(USER_ROLE);
            return roles;
        }
        List<Long> roleIds = roleDtos.stream().map(RoleDto::getId).collect(Collectors.toList());
        roles = findByIds(roleIds);
        if (roles.isEmpty()) {
            NoIdFoundException exc = new NoIdFoundException(Role.class, roleIds);
            log.error("Role service: " + exc.getMessage());
            throw exc;
        }
        log.info("Role service: roles have been successfully parsed");
        return roles;
    }
}