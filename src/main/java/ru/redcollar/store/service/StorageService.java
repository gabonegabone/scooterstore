package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.redcollar.store.dto.storage.StorageDto;
import ru.redcollar.store.entity.Storage;
import ru.redcollar.store.exception.AlreadyExistException;
import ru.redcollar.store.exception.NoContentException;
import ru.redcollar.store.exception.NoIdFoundException;
import ru.redcollar.store.mapper.StorageMapper;
import ru.redcollar.store.repository.StorageRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StorageService {

    private final StorageRepository storageRepository;
    private final StorageMapper storageMapper;

    public List<StorageDto> getList() {
        List<Storage> storages = storageRepository.findAll();
        if (storages.isEmpty()) {
            NoContentException exc = new NoContentException(Storage.class);
            log.error("Storage service: " + exc.getMessage());
            throw exc;
        }
        log.info("Storage service: getList() - success");
        return storages
                .stream()
                .map(storageMapper::parseToDto)
                .collect(Collectors.toList());
    }

    public StorageDto save(StorageDto newStorage) {
        if (storageRepository.existsById(newStorage.getId())) {
            AlreadyExistException exc = new AlreadyExistException(Storage.class);
            log.error("Storage service: " + exc.getMessage());
            throw exc;
        }
        Storage storage = storageMapper.parseToEntity(newStorage);
        storage = storageRepository.save(storage);
        log.info("Storage service: Storage with id {} successfully created", storage.getId());
        return storageMapper.parseToDto(storage);
    }

    public StorageDto update(StorageDto storageDto) {
        if (!storageRepository.existsById(storageDto.getId())) {
            NoIdFoundException exc = new NoIdFoundException(Storage.class, storageDto.getId());
            log.error("Storage service: " + exc.getMessage());
            throw exc;
        }
        Storage storage = storageMapper.parseToEntity(storageDto);
        storage = storageRepository.save(storage);
        log.info("Storage service: Storage with id {} successfully edited", storage.getId());
        return storageMapper.parseToDto(storage);
    }

    public void delete(Long id) {
        if (!storageRepository.existsById(id)) {
            NoIdFoundException exc = new NoIdFoundException(Storage.class, id);
            log.error("Storage service: " + exc.getMessage());
            throw exc;
        }
        log.info("Storage service: Storage with id {} successfully deleted", id);
        storageRepository.deleteById(id);
    }
}
