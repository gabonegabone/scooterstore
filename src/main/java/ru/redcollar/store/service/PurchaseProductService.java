package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.redcollar.store.dto.purchaseproduct.PurchaseProductDto;
import ru.redcollar.store.entity.Product;
import ru.redcollar.store.entity.Purchase;
import ru.redcollar.store.entity.PurchaseProduct;
import ru.redcollar.store.mapper.PurchaseProductMapper;
import ru.redcollar.store.repository.ProductRepository;
import ru.redcollar.store.repository.PurchaseProductRepository;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PurchaseProductService {

    private final PurchaseProductRepository purchaseProductRepository;
    private final ProductRepository productRepository;
    private final PurchaseProductMapper purchaseProductMapper;

    @Transactional
    public List<PurchaseProduct> saveListForPurchase(List<PurchaseProductDto> dtos, Long purchaseId) {
        List<PurchaseProduct> purchaseProducts = dtos
                .stream().map(purchaseProductMapper::parseToEntity)
                .collect(Collectors.toList());
        purchaseProducts.forEach(x -> x.setPurchase(new Purchase(purchaseId)));
        purchaseProducts = purchaseProductRepository.saveAllAndFlush(purchaseProducts);
        log.info("PurchaseProduct service: Product's for Purchase with id {} have been saved", purchaseId);
        return purchaseProducts;
    }

    public List<PurchaseProduct> setProducts(List<PurchaseProduct> purchaseProducts) {
        List<Long> productsId =
                purchaseProducts.stream().map(x -> x.getProduct().getId()).collect(Collectors.toList());
        List<Product> products = productRepository.findAllById(productsId);

        Iterator<PurchaseProduct> ppi = purchaseProducts.iterator();
        Iterator<Product> pi = products.iterator();
        while (ppi.hasNext() & pi.hasNext()) {
            ppi.next().setProduct(pi.next());
        }
        return purchaseProducts;
    }
}
