package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.redcollar.store.dto.LoginFormDto;
import ru.redcollar.store.dto.TokenDto;
import ru.redcollar.store.dto.account.AccountCreationDto;
import ru.redcollar.store.dto.account.AccountDto;
import ru.redcollar.store.entity.Account;
import ru.redcollar.store.mapper.AccountMapper;
import ru.redcollar.store.repository.AccountRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {

    private final AccountService accountService;
    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtService jwtService;

    public TokenDto login(LoginFormDto loginFormDto) {
        Account account = accountRepository.findByEmail(loginFormDto.getEmail());
        if (account == null ||
                !bCryptPasswordEncoder.matches(loginFormDto.getPassword(), account.getPassword())) {
            BadCredentialsException exc = new BadCredentialsException("bad credentials");
            log.error("Authentication Service: " + exc.getMessage());
            throw exc;
        }
        AccountDto accountDto = accountMapper.parseToDto(account);
        String token = jwtService.generateJwt(accountDto);
        log.info("Authentication Service: Account with id {} successfully proceeded authorization",
                account.getId());
        return new TokenDto(token);
    }

    public void register(AccountCreationDto newAccount) {
        AccountDto account = accountService.createAccount(newAccount);
        log.info("Authentication Service: Account with id {} successfully registered", account.getId());
    }
}
