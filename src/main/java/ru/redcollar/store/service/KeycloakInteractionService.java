package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import ru.redcollar.store.dto.KeycloakToken;
import ru.redcollar.store.dto.KeycloakUser;
import ru.redcollar.store.entity.Purchase;

@Service
@Slf4j
@RequiredArgsConstructor
public class KeycloakInteractionService {

    private static final String KEYCLOAK_MS_STATUS_UPD_URI = "http://keycloak-service/msg/upd-status";
//    private static final String KEYCLOAK_MS_STATUS_UPD_URI = "http://localhost:8082/msg/upd-status";

    @Value("${keycloak.credentials.admin.login}")
    private String adminUsername;

    @Value("${keycloak.credentials.admin.password}")
    private String adminPassword;

    private final KeycloakAuthService keycloakAuthService;

    @LoadBalanced
    private final RestTemplate restTemplate;

    public void sendMsg(Purchase.Status status) {
        KeycloakUser admin = new KeycloakUser(adminUsername, adminPassword);
        KeycloakToken token = keycloakAuthService.getToken(admin);
        log.debug("KeycloakInteractionService: token from auth/token {}", token);

        UriComponents uriComponents = UriComponentsBuilder
                .fromUriString(KEYCLOAK_MS_STATUS_UPD_URI)
                .queryParam("status", status)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + token.getAccessToken());
        HttpEntity<String> request = new HttpEntity<>(headers);
        restTemplate.postForEntity(uriComponents.toUri(), request, ResponseEntity.class);
    }
}
