package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import ru.redcollar.store.dto.purchase.PurchaseDto;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageSenderService {

    private static final String ROUTING_KEY_NEW_ORDER = "purchase.new";

    private final TopicExchange fanoutExchange;

    private final RabbitTemplate rabbitTemplate;

    public void sendOrderDetails(PurchaseDto purchase) {
        rabbitTemplate.convertAndSend(fanoutExchange.getName(), ROUTING_KEY_NEW_ORDER, purchase);
        log.info("[x] message \"{}\" sent!", purchase);
        log.info("end of sendOrderDetailsMethod");
    }
}
