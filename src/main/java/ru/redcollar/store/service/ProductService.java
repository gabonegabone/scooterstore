package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.redcollar.store.dto.product.ProductDto;
import ru.redcollar.store.entity.Product;
import ru.redcollar.store.exception.AlreadyExistException;
import ru.redcollar.store.exception.NoContentException;
import ru.redcollar.store.exception.NoIdFoundException;
import ru.redcollar.store.mapper.ProductMapper;
import ru.redcollar.store.repository.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public List<ProductDto> getList() {
        List<Product> products = productRepository.findAll();
        if (products.isEmpty()) {
            NoContentException exc = new NoContentException(Product.class);
            log.error("Product service: " + exc.getMessage());
            throw exc;
        }
        log.info("Product service: getList() - success");
        return products.stream().map(productMapper::parseToDto)
                .collect(Collectors.toList());
    }

    public ProductDto create(ProductDto newProduct) {
        Product product = productMapper.parseToProduct(newProduct);
        if (productRepository.existByName(product.getName())) {
            AlreadyExistException exc = new AlreadyExistException(Product.class);
            log.error("Product service: " + exc.getMessage());
            throw exc;
        }
        product = productRepository.save(product);
        log.info("Product service: Product with id {} successfully created", product.getId());
        return productMapper.parseToDto(product);
    }

    public ProductDto edit(ProductDto productDto) {
        if (!productRepository.existsById(productDto.getId())) {
            NoIdFoundException exc = new NoIdFoundException(Product.class, productDto.getId());
            log.error("Product service: " + exc.getMessage());
            throw exc;
        }
        Product product = productMapper.parseToProduct(productDto);
        product = productRepository.save(product);
        log.info("Product service: Product with id {} successfully edited", product.getId());
        return productMapper.parseToDto(product);
    }

    public void delete(Long id) {
        if (!productRepository.existsById(id)) {
            NoIdFoundException exc = new NoIdFoundException(Product.class, id);
            log.error("Product service: " + exc.getMessage());
            throw exc;
        }
        log.info("Product service: Product with id {} successfully deleted", id);
        productRepository.deleteById(id);
    }
}
