package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.redcollar.store.dto.KeycloakToken;
import ru.redcollar.store.dto.KeycloakUser;

@Service
@Slf4j
@RequiredArgsConstructor
public class KeycloakAuthService {

    private static final String KEYCLOAK_MS_AUTH_URI = "http://keycloak-service/auth/%s";
//    private static final String KEYCLOAK_MS_AUTH_URI = "http://localhost:8082/auth/%s";

    @LoadBalanced
    private final RestTemplate restTemplate;

    @Cacheable(value = "token", key = "#user")
    public KeycloakToken getToken(KeycloakUser user) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<KeycloakUser> request = new HttpEntity<>(user, headers);
        String uri = String.format(KEYCLOAK_MS_AUTH_URI, "token");
        KeycloakToken token = restTemplate.postForObject(uri, request, KeycloakToken.class);
        if (token == null) {
            String msg = "KeycloakInteractionService: getToken: produced null token";
            log.error(msg);
            throw new RuntimeException(msg);
        }
        log.info("KeycloakInteractionService: getToken: token {}", token);
        return token;
    }

    @CachePut(value = "token", key = "#user") //#root.args[1]
    public KeycloakToken updateToken(KeycloakToken token, KeycloakUser user) {
        if (token == null) {
            String msg = "KeycloakInteractionService: updateToken: token is null";
            log.error(msg);
            throw new NullPointerException(msg);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<KeycloakToken> request = new HttpEntity<>(token, headers);
        String uri = String.format(KEYCLOAK_MS_AUTH_URI, "refresh");
        KeycloakToken updatedToken = restTemplate.postForObject(uri, request, KeycloakToken.class);
        if (updatedToken == null) {
            String msg = "KeycloakInteractionService: getToken: produced null token";
            log.error(msg);
            throw new RuntimeException(msg);
        }
        log.info("KeycloakInteractionService: updatedToken: token {}", updatedToken);
        return updatedToken;
    }
}
