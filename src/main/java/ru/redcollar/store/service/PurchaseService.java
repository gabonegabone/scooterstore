package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.redcollar.store.dto.purchase.PurchaseDto;
import ru.redcollar.store.entity.Account;
import ru.redcollar.store.entity.Purchase;
import ru.redcollar.store.entity.PurchaseProduct;
import ru.redcollar.store.exception.NoContentException;
import ru.redcollar.store.exception.NoIdFoundException;
import ru.redcollar.store.mapper.PurchaseMapper;
import ru.redcollar.store.pagination.PurchasePaginationSpecifications;
import ru.redcollar.store.pagination.PurchaseSpecificationBuilder;
import ru.redcollar.store.repository.AccountRepository;
import ru.redcollar.store.repository.PurchaseRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PurchaseService {

    private final PurchaseRepository purchaseRepository;
    private final AccountRepository accountRepository;
    private final PurchaseMapper purchaseMapper;
    private final PurchaseProductService purchaseProductService;
    private final MessageSenderService messageSenderService;
    private final KeycloakInteractionService keycloakInteractionService;

    public List<PurchaseDto> getList(PurchasePaginationSpecifications filter) {
        Pageable pageable = PageRequest.of(filter.getPage(), filter.getSize(), filter.getDirection(), filter.getSortBy());
        Specification<Purchase> specification = PurchaseSpecificationBuilder.build(filter);
        List<Long> purchaseIds = purchaseRepository.findPurchasesIds(specification, pageable);
        List<Purchase> purchases = purchaseRepository.findAllByIds(purchaseIds);
        if (purchases.isEmpty()) {
            NoContentException exc = new NoContentException(Purchase.class);
            log.error("Purchase service: " + exc.getMessage());
            throw exc;
        }
        log.info("Purchase service: getList() - success");
        return purchases
                .stream()
                .map(purchaseMapper::parseToDto)
                .collect(Collectors.toList());
    }

    public PurchaseDto edit(PurchaseDto purchaseDto) {
        if (!purchaseRepository.existsById(purchaseDto.getId())) {
            NoIdFoundException exc = new NoIdFoundException(Purchase.class, purchaseDto.getId());
            log.error("Purchase service: " + exc.getMessage());
            throw exc;
        }
        Purchase purchase = purchaseMapper.parseToEntity(purchaseDto);
        purchase = purchaseRepository.save(purchase);
        keycloakInteractionService.sendMsg(purchase.getStatus());
        log.info("Purchase service: Purchase with id {} successfully edited", purchase.getId());
        return purchaseMapper.parseToDto(purchase);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public PurchaseDto checkout(PurchaseDto purchaseDto) {
        var authentication =
                (UsernamePasswordAuthenticationToken) SecurityContextHolder
                        .getContext().getAuthentication();
        var accountId = (Long) authentication.getPrincipal();
        Account account = accountRepository.getById(accountId);
        Purchase purchase = new Purchase();
        purchase.setAccount(account);
        purchase.setCheckoutDate(LocalDate.now());
        purchase.setStatus(Purchase.Status.PROCESSING);
        purchase = purchaseRepository.saveAndFlush(purchase);
        Long purchaseId = purchase.getId();
        List<PurchaseProduct> purchaseProducts =
                purchaseProductService.saveListForPurchase(purchaseDto.getPurchaseProducts(), purchaseId);
        purchaseProducts = purchaseProductService.setProducts(purchaseProducts);
        purchase.setPurchaseProducts(purchaseProducts);
        log.info("Purchase service: Purchase with id {} successfully created", purchaseId);
        PurchaseDto dto = purchaseMapper.parseToDto(purchase);
        messageSenderService.sendOrderDetails(dto);
        log.info("PurchaseService: called MessageSenderService: sendOrderDetails...");
        return dto;
    }

}
