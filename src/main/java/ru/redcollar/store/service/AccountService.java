package ru.redcollar.store.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.redcollar.store.dto.account.AccountCreationDto;
import ru.redcollar.store.dto.account.AccountDto;
import ru.redcollar.store.dto.account.AccountPasswordEditingDto;
import ru.redcollar.store.dto.role.RoleDto;
import ru.redcollar.store.entity.Account;
import ru.redcollar.store.entity.Role;
import ru.redcollar.store.exception.AlreadyExistException;
import ru.redcollar.store.exception.NoContentException;
import ru.redcollar.store.exception.NoIdFoundException;
import ru.redcollar.store.exception.PasswordMatchingException;
import ru.redcollar.store.mapper.AccountMapper;
import ru.redcollar.store.repository.AccountRepository;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    public List<AccountDto> getList() {
        List<Account> list = accountRepository.findAll();
        if (list.isEmpty()) {
            NoContentException exc = new NoContentException(Account.class);
            log.error("Account service: " + exc.getMessage());
            throw exc;
        }
        log.info("Account service: getList() - success");
        return list.stream().map(accountMapper::parseToDto).collect(Collectors.toList());
    }

    public AccountDto getAccount(Long id) {
        Account account = accountRepository.getById(id);
        log.info("Account service: getAccount(id: {}) - success", id);
        return accountMapper.parseToDto(account);
    }

    public AccountDto editAccount(AccountDto newAccount) {
        if (!accountRepository.existsById(newAccount.getId())) {
            NoIdFoundException exc = new NoIdFoundException(Account.class, newAccount.getId());
            log.error("Account service: " + exc.getMessage());
            throw exc;
        }
        List<Long> roleIds = newAccount.getRoles().stream().map(RoleDto::getId).collect(Collectors.toList());
        List<Role> roles = roleService.findByIds(roleIds);
        if (roles.isEmpty()) {
            NoIdFoundException exc = new NoIdFoundException(Role.class, roleIds);
            log.error("Account service: " + exc.getMessage());
            throw exc;
        }
        Account account = accountMapper.parseToEntity(newAccount);
        account.setRoles(roles);
        account = accountRepository.save(account);
        log.info("Account service: Account with id {} successfully edited", account.getId());
        return accountMapper.parseToDto(account);
    }

    public AccountDto createAccount(AccountCreationDto newAccount) {
        if (accountRepository.existsByEmail(newAccount.getEmail())) {
            AlreadyExistException exc = new AlreadyExistException(Account.class);
            log.error("Account service: " + exc.getMessage());
            throw exc;
        }
        List<RoleDto> roleDtos = newAccount.getRoles();
        List<Role> roles = roleService.parseRoles(roleDtos);
        Account account = accountMapper.parseToEntity(newAccount);
        account.setRoles(roles);
        account.setPassword(passwordEncoder.encode(newAccount.getPassword()));
        account = accountRepository.save(account);
        log.info("Account service: Account with id {} successfully created", account.getId());
        return accountMapper.parseToDto(account);
    }

    public AccountDto editPassword(AccountPasswordEditingDto newAccount) {
        if (!newAccount.getPassword().equals(newAccount.getPasswordRepeat())) {
            PasswordMatchingException exc = new PasswordMatchingException();
            log.error("Account service: " + exc.getMessage());
            throw exc;
        }
        Account account = accountRepository.findById(newAccount.getId()).orElseThrow(() -> {
            NoIdFoundException exc = new NoIdFoundException(Account.class, newAccount.getId());
            log.error("Account service: " + exc.getMessage());
            throw exc;
        });
        account.setPassword(newAccount.getPassword());
        account = accountRepository.save(account);
        log.info("Account service: password for Account with id {} successfully edited", account.getId());
        return accountMapper.parseToDto(account);
    }

    public void deleteAccount(Long id) {
        if (!accountRepository.existsById(id)) {
            NoIdFoundException exc = new NoIdFoundException(Account.class, id);
            log.error("Account service: " + exc.getMessage());
            throw exc;
        }
        log.info("Account service: Account with id {} successfully deleted", id);
        accountRepository.deleteById(id);
    }
}