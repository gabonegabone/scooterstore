package ru.redcollar.store.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.redcollar.store.dto.account.AccountDto;

@Service
@RequiredArgsConstructor
@Slf4j
public class JwtService {

    private final ObjectMapper objectMapper;

    @Value("${token.secret}")
    private String secret;

    public String generateJwt(AccountDto accountDto) {
        log.info("Token created");
        return Jwts.builder()
                .claim("account", accountDto)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public <T> T parseJwt(String token, Class<T> requiredType) {
        Claims body = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        return objectMapper.convertValue(body.get("account"), requiredType);
    }
}
