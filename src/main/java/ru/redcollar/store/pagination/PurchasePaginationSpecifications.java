package ru.redcollar.store.pagination;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class PurchasePaginationSpecifications {

    @NotNull(message = "page property must not be null")
    private Integer page;

    @NotNull(message = "size property must not be null")
    private Integer size;

    private Sort.Direction direction = Sort.Direction.ASC;
    private String sortBy  = "id";

    private LocalDate fromDate;
    private LocalDate toDate;
    private String productName;
}
