package ru.redcollar.store.pagination;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import ru.redcollar.store.entity.Purchase;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class PurchaseSpecificationBuilder {

    public static Specification<Purchase> build(PurchasePaginationSpecifications filter) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (filter.getToDate() != null) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("checkoutDate"), filter.getToDate()));
            }
            if (filter.getFromDate() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("checkoutDate"), filter.getFromDate()));
            }
            if (StringUtils.isNotBlank(filter.getProductName())) {
                predicates.add(criteriaBuilder.like(root.join("purchaseProducts").join("product").get("name"), filter.getProductName()));
            }
            return criteriaBuilder.and(predicates.toArray(Predicate[]::new));
        };
    }
}
