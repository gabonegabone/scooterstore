package ru.redcollar.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.redcollar.store.entity.PurchaseProduct;

public interface PurchaseProductRepository extends JpaRepository<PurchaseProduct, Long> {
}
