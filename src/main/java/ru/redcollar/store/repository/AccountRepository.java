package ru.redcollar.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.redcollar.store.entity.Account;

import java.util.List;


@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query(
            "select account from Account account left join fetch account.roles where account.email = (:email)"
    )
    Account findByEmail(String email);

    @Query(
            "select account from Account account left join fetch account.roles order by account.id asc"
    )
    List<Account> findAll();

    @Query(
            "select case when count (account) > 0 then true else false end" +
                    " from Account account where account.email = (:email) "
    )
    boolean existsByEmail(String email);
}
