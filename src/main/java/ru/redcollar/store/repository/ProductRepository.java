package ru.redcollar.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.redcollar.store.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(
            "select case when count(product) > 0 then true else false end " +
                    "from Product product where product.name = (:name)"
    )
    boolean existByName(String name);
}
