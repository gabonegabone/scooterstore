package ru.redcollar.store.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.Nullable;
import ru.redcollar.store.entity.Purchase;

import java.util.List;

public interface PurchaseRepository extends JpaRepository<Purchase, Long>, JpaSpecificationExecutor<Purchase> {

    @Query(
            value = "select distinct purchase from Purchase purchase " +
                    "left join fetch purchase.purchaseProducts as purchProd " +
                    "left join fetch purchProd.product as prod " +
                    "left join fetch purchase.account " +
                    "where purchase.id in (:ids)"
    )
    List<Purchase> findAllByIds(Iterable<Long> ids);

    @Query(
            value = "select purchase.id from Purchase purchase"
    )
    List<Long> findPurchasesIds(@Nullable Specification<Purchase> spec, @Nullable Pageable pageable);
}
