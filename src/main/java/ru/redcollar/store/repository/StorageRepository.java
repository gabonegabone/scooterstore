package ru.redcollar.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.redcollar.store.entity.Storage;

import java.util.List;

public interface StorageRepository extends JpaRepository<Storage, Long> {

    @Query(
            "select distinct storage from Storage storage join fetch storage.storageProducts as products join fetch products.product"
    )
    List<Storage> findAll();
}
