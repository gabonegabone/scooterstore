package ru.redcollar.store.exception;

import java.util.List;

public class NoIdFoundException extends RuntimeException {

    public NoIdFoundException(String message) {
        super(message);
    }

    public <T> NoIdFoundException(Class<T> clazz, Long id) {
        super(
                "No " + clazz.getName() + "has been found with id " + id
        );
    }

    public <T> NoIdFoundException(Class<T> clazz, List<Long> ids) {
        super(
                "No " + clazz.getName() + "has been found with ids " + ids
        );
    }
}