package ru.redcollar.store.exception;

public class AlreadyExistException extends RuntimeException{
    public <T> AlreadyExistException(Class<T> clazz) {
        super("Same object \"" + clazz.getName() + "\" already exists");
    }
}
