package ru.redcollar.store.exception;

public class PasswordMatchingException extends RuntimeException {

    public PasswordMatchingException() {
        super("Password fields do not match");
    }
}
