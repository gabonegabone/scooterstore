package ru.redcollar.store.exception;

public class NoContentException extends RuntimeException {
    public <T> NoContentException(Class<T> clazz) {
        super(
                "No content for " + clazz.getName() + " has been found"
        );
    }
}
