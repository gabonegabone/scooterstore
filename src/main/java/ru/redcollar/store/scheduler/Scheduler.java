package ru.redcollar.store.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.redcollar.store.dto.KeycloakToken;
import ru.redcollar.store.dto.KeycloakUser;
import ru.redcollar.store.service.KeycloakAuthService;
import ru.redcollar.store.service.KeycloakInteractionService;

import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@RequiredArgsConstructor
@Component
public class Scheduler {

    private static final String CACHE_NAME = "token";

    private final KeycloakAuthService keycloakAuthService;
    private final CacheManager cacheManager;

    //repeated every 45 seconds
    @Scheduled(cron = "*/45 * * * * ?")
    public void updateToken() {
        log.info("SCHEDULING DOING JOB");
        Cache cache = cacheManager.getCache(CACHE_NAME);
        if (cache == null) {
            log.info("cache for name={} is empty", CACHE_NAME);
            return;
        }
        var nativeCache = (ConcurrentHashMap<KeycloakUser, KeycloakToken>) cache.getNativeCache();
        for (KeycloakUser user: nativeCache.keySet()) {
            log.info("key={}", user);
            KeycloakToken token = nativeCache.get(user);
            log.info("value={}", token);
            keycloakAuthService.updateToken(token, user);
        }
        log.info("SCHEDULING DONE");
    }
}
