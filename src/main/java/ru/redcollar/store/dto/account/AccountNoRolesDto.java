package ru.redcollar.store.dto.account;

import lombok.Data;

@Data
public class AccountNoRolesDto {

    private Long id;
    private String name;
    private String email;
}
