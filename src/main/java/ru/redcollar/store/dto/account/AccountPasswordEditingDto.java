package ru.redcollar.store.dto.account;

import lombok.Data;

@Data
public class AccountPasswordEditingDto {

    private Long id;

    private String password;

    private String passwordRepeat;
}