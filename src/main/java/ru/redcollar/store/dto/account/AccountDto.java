package ru.redcollar.store.dto.account;

import lombok.Data;
import ru.redcollar.store.dto.role.RoleDto;

import java.util.List;

@Data
public class AccountDto {

    private Long id;

    private String name;

    private String email;

    private List<RoleDto> roles;
}