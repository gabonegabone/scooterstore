package ru.redcollar.store.dto.account;

import lombok.Data;

@Data
public class AccountCreationDto extends AccountDto {

    private String password;

}
