package ru.redcollar.store.dto.storage;

import lombok.Data;
import ru.redcollar.store.dto.product.ProductShortDto;

@Data
public class StorageProductDto {

    private ProductShortDto storage;
    private Integer quantity;
}
