package ru.redcollar.store.dto.storage;

import lombok.Data;

import java.util.List;

@Data
public class StorageDto {

    private Long id;
    private List<StorageProductDto> products;
    private String info;
}
