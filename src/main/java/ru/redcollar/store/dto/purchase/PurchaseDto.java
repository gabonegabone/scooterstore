package ru.redcollar.store.dto.purchase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.redcollar.store.dto.account.AccountNoRolesDto;
import ru.redcollar.store.dto.purchaseproduct.PurchaseProductDto;
import ru.redcollar.store.entity.Purchase;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDto implements Serializable {

    private Long id;
    private AccountNoRolesDto account;
    private List<PurchaseProductDto> purchaseProducts;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate checkoutDate;
    private Purchase.Status status;
}
