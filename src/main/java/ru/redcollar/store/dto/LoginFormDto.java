package ru.redcollar.store.dto;

import lombok.Data;

@Data
public class LoginFormDto {

    private String email;

    private String password;
}
