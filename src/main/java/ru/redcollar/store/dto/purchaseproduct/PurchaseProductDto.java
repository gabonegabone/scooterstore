package ru.redcollar.store.dto.purchaseproduct;

import lombok.Data;
import ru.redcollar.store.dto.product.ProductDto;

@Data
public class PurchaseProductDto {

    private ProductDto product;
    private Integer quantity;
}
