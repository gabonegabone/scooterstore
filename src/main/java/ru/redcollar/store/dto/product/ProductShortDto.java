package ru.redcollar.store.dto.product;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductShortDto {

    private Long id;
    private String name;
    private BigDecimal price;
}
