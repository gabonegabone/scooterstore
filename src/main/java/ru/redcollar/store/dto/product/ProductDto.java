package ru.redcollar.store.dto.product;

import lombok.Data;
import ru.redcollar.store.entity.Product;

import java.math.BigDecimal;

@Data
public class ProductDto {

    private Long id;
    private String name;
    private BigDecimal price;
    private Product.Color color;
    private Integer barHeight;
    private Integer deckLength;
    private Integer deckWidth;
    private Integer wheelDiameter;
    private Integer wheelWidth;
    private Product.Compression compression;
    private Product.Material material;
    private Boolean barsBacksweep;
}
